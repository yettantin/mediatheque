const fram_express = require("express");
const router = fram_express.Router();
const newLivre = require("../models/m_mediatheque"); //Importe les variables exportées
const multer = require("multer");
const fs = require("fs");

// configuration multer
// ====================
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/image/tmp/')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })
  const upload = multer({ storage: storage })
  
// home
// ====
router.get("/", function(req, res) {
    res.render("v_home");
})

// Liste complète
// ==============
router.get("/all", function(req, res) {
    //cherche dans la base de donnée la liste complète des livres
    newLivre.find({}, function(err, listeComplete) { //.find = méthode de mongoose, listeComplete = variable temporaire qui stocke les données récupérées dans la BDD
        if(err) res.send(err)
        res.render("v_listeComplete", {listeLivre: listeComplete});
    })
})

// ajouter un nouveau livre
// ========================
router.route("/ajout")
    .get(function(req, res) {
    res.render("v_ajout")
    })

    .post(function(req, res) {
        const newlivreSchema = new newLivre(req.body)
        console.log(req.body);
        newlivreSchema.save(function(err, livreX) {    
            if(err) res.send(err);
            res.redirect('/couverture/'+ livreX._id);
        })
    })
router.route("/couverture/:id")
    .get(function (req, res) {
        newLivre.find({_id: req.params.id}, function(err, livreX) { //.find = méthode de mongoDb, listeComplete = variable temporaire qui stocke les données récupérées dans la BDD
            if(err) res.send(err)
            res.render("v_imageLivre", {livre: livreX[0]})
        })
    })
    .post(upload.single('pochetteLivre'), function (req, res, next) {
        const nomFichier = req.file.filename.split('.');
        const extension = nomFichier[nomFichier.length -1]
        fs.rename("./public/image/tmp/"+req.file.filename, "./public/image/pochette/"+ req.params.id + "." + extension, function(err) {
            if(err) res.send(err);
            newLivre.updateOne({_id: req.params.id}, {imagePochette: req.params.id + "." + extension} , function(err) {
                if (err) throw err;
                res.redirect('/description/'+req.params.id);
            })            
        })       
    })
        

// modification des données d'un livre
// ===================================
router.route("/modification/:id")
    .get(function(req,res) {
        newLivre.find({_id: req.params.id}, function(err, livreX) { 
            if(err) res.send(err)
            res.render("v_modification", {modifLivre: livreX[0]})   
        })
    })
    .post(function(req,res) {
        const livreModifie = req.body
        const livreId = req.params.id
        newLivre.updateOne({_id: livreId}, livreModifie , function(err) {   //on pourrait aussi utiliser objectAssign()
            if (err) throw err;
            console.log("1 document updated");
            res.redirect('/couverture/'+ livreId);
        })
    })

// Détail/description d'un livre 
// =============================
router.get("/description/:id", function(req, res) {   
    newLivre.find({_id: req.params.id}, function(err, livreX) { // ATTENTION :livreX est un tableau
        if(err) res.send(err)
        res.render("v_description", {descLivre: livreX[0]});    //[0] = on choisi un élément du tableau de façon a le sortir du tableau. sinon, il ressortirait le tableau complet.
    })
})

// supprimer un livre
// ==================
router.post("/description/:id", function(req, res) {
    const livreId = req.params.id
    newLivre.findOne({_id : req.params.id},function(err,livreX){        // a essayer : .findById()
        if(err) res.send(err);
        fs.unlink('./public/image/pochette/'+livreX.imagePochette ,function(err){
            if(err) console.log(err);
            console.log('file deleted successfully');
        })
    })
    newLivre.deleteOne({_id: livreId}, function(err) {
        if (err) throw err;
        console.log("1 document effacé");
        res.redirect('/all');        
    })
})

module.exports = router;