const fram_express = require("express");
const interface_com = fram_express();
const router = require("./routes/routes")
const interface_mongoose = require('mongoose');
const config = require('./config/default')

const port = process.env.PORT || 5000;

//Configuration
interface_com.use(fram_express.static(__dirname + "/public"));
interface_com.use(fram_express.urlencoded({extended: false}));    //ATTENTION : bien mettre le body-parser avant les routes
interface_com.set("view engine", "pug");

//connection à la base de donnée mongodb
interface_mongoose.connect(config.DBHost,{useNewUrlParser: true}); //mediatheque_db = nom de la base de donnée, mettre le user et password dans le dissier config qui n'est pas gité permet de protéger le password
const db = interface_mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("c'est bon! La BD est connectée")
});

//router
interface_com.use("/", router);

//écoute des clients
interface_com.listen(port, function() {
    console.log("[ça marche]")
})