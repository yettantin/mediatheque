const interface_mongoose = require("mongoose");
const Schema = interface_mongoose.Schema;

const livreSchema = new Schema ({
    titre:{type: String, required : true},
    auteur:{type: String, required : true},
    nb_pages: {type: Number, required : true},
    date_pub: {type: String, required : true},
    genre: {type: String, required : true},
    synopsis: {type: String, required : true},
    imagePochette: {type: String, required : true},
    creatAt: Date    
});

livreSchema.pre("save", next => {
    now = new Date();
    if(!this.createAt) {
        this.createAt = now;
    };
    next();
});

const newLivre = interface_mongoose.model("livre", livreSchema); //"livre" = nom de l'objet stocké dans la BDD

module.exports = newLivre;